# {{title}}

{{description}}

---

## Agenda

* Sandi Metz's testing tricks
* Give meaning to your UT (aka. the *describe* ritual)
* Code some Ruby
* Learn how to sprout beans

---

## Sandi Metz's magic tricks

![Minimalist testing table](img/minimalist-testing.png "Minimalist testing table")

Note:
Insist on Command VS Query messages
We won't cover the full scope of that
If you haven't washed you're missing out

---

## Sprouting beans...


Scenario

> I want to *Sprout some beans*.

Model

    Batch
      --> Contains beans
      --> 2 states: soaked, drained
      --> soaking and draining the batch will affect beans' states

<!-- -->

    Bean
      --> 2 states: sprouted, unsprouted

Note:
Should we get going and write the classes?
Yes? Bad answer! Tests first.

---

## Tests first!

```ruby
class BatchTest < Minitest::Test

  def setup do
    @batch = Batch.new
  end

  def test_has_beans do
    refute_nil @batch.beans
    assert_equals @batch.beans.first, "bean_0"
  end

  def test_becomes_soaked do
    @batch.soak
    assert_equals @batch.state, "soaked"
  end
end
```

----

## Tests first (with BDD sauce)

```ruby
describe Batch do
  let(:batch) { Batch.new }

  it "has some beans" do
    batch.beans.wont_be_nil
    batch.beans.first.must_equal "bean_0"
  end

  it "changes the batch state to soaked" do
    batch.soak
    batch.state.must_equal "soaked"
  end
end
```

Note: Give it a run
fragments in code: http://stackoverflow.com/questions/24151563/reveal-js-add-fragments-inside-code

---

## Just code it!

```ruby
class Batch
  attr_accessor :beans, :state

  def initialize
    self.beans = find_beans
  end

  def soak
    self.state = "soaked"
  end

  private
  def find_beans
    Array.new(rand(60..100)) { |m| "bean_#{m}"}
  end
end

```

Note:
Comment on asserting public side effect of a private method

---

### Adding some real Beans (1/2)

```ruby
describe Bean do
  let(:bean) { Bean.new }

  it "is unsprouted by default" do
    bean.state.must_equal "unsprouted"
  end

  it "sprouts" do
    bean.sprout
    bean.state.must_equal "sprouted"
  end
end
```

----

### Adding some real Beans (2/2)

Updating the Batch methods:

```ruby
def find_beans
  Array.new(rand(60..100)) { Bean.new }
end
```
<!-- -->

```ruby
def drain
  self.state = "drained"
  self.beans.map { |b| b.sprout }
end
```

Note:
Ask them to write the resulting Bean class if time

----

### Resulting Bean class

```ruby
class Bean
  attr_accessor :state

  def initialize
    self.state = "unsprouted"
  end

  def sprout
    self.state = "sprouted" if should_sprout
  end

  private
  def should_sprout
    [true, false].sample
  end
end
```

---

## Group questions and exercises (1/2)

* Should you add a test for the should_sprout method? Why?

* Due to our latest changes, the following test now fails:

```ruby
it "has some beans" do
  batch.beans.wont_be_nil
  batch.beans.first.must_equal "bean_0"
end
```

Make it pass ([Cheat sheet](http://cheat.errtheblog.com/s/minitest)).

----

## Group questions and exercises (2/2)

* In the current implementation, our test for the *sprout* method will randomly fail. Why is that and how to fix it?

---

## Thank you :)

Note:
http://jamesmead.org/talks/2007-07-09-introduction-to-mock-objects-in-ruby-at-lrug/
